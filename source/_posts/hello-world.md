---
title: hello-world
date: 2020-01-09 05:49:49
tags:
---
Every blog, every program, every project needs a 'hello world' post or line.
This is that post.

_Hello world._
_Oh I'm marvelous, how about you?_
